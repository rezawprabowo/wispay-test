import Home from 'app/screens/Home'
import Detail from 'app/screens/Detail'

import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

const navStack = createStackNavigator(
   {
      Home: {
         screen: Home,
         navigationOptions: { headerShown: null }
      },
      Detail: {
         screen: Detail,
         navigationOptions: { headerShown: null }
      },
   },
   {
      initialRouteName: 'Home'
   }
)

const navSwitch = createSwitchNavigator(
   {
      Main: {
         screen: navStack,
         navigationOptions: { headerShown: null }
      }
   },
   {
      initialRouteName: 'Main'
   }
)

export default createAppContainer(navSwitch)