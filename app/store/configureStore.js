import { createStore, compose, applyMiddleware } from 'redux'
import { persistStore, persistCombineReducers } from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage'

import rootReducers from 'app/reducers'

const config = {
   key: 'root',
   storage: AsyncStorage,
   blacklist: ['nav', 'loadingReducer'],
   debug: true
}

const middleware = []
const reducers = persistCombineReducers(config, rootReducers)
const enhancers = [applyMiddleware(...middleware)]
const persistConfig = { enhancers }
const store = createStore(reducers, undefined, compose(...enhancers))
const persistor = persistStore(store, persistConfig)

const configureStore = () => {
   return { persistor, store }
}

export default configureStore