import React from 'react'
import { StyleSheet, Text } from 'react-native'

const ComponentText = props => {
   return (
      <Text
         { ...props }
         style={[
            props.bold     ? styles.bold     : styles.normal,
            props.small    ? styles.small    : null,
            props.medium   ? styles.medium   : null,
            props.big      ? styles.big      : null,
            props.sub      ? styles.sub      : null,
            props.title    ? styles.title    : null,
            props.number   ? styles.number   : null,
            props.jumbo    ? styles.jumbo    : null,
            props.white    ? styles.white    : null,
            props.grey     ? styles.grey     : null,
            props.theme     ? styles.theme   : null,
            props.style
         ]}
      >
         { props.text }
      </Text>
   )
}

ComponentText.defaultProps = {
   bold     : false,
   small    : false,
   medium   : false,
   big      : false,
   sub      : false,
   title    : false,
   number   : false,
   jumbo    : false,
   white    : false,
   grey     : false,
   theme    : false
}

module.exports = ComponentText

const styles = StyleSheet.create({
   bold: {
      fontFamily: 'MontB',
      color: '#000000'
   },

   normal: {
      fontFamily: 'Mont',
      color: '#000000'
   },

   small: {
      fontSize: 10
   },

   medium: {
      fontSize: 12
   },

   big: {
      fontSize: 14
   },

   sub: {
      fontSize: 16
   },

   title: {
      fontSize: 18
   },

   number: {
      fontSize: 22
   },

   jumbo: {
      fontSize: 28
   },

   white: {
      color: '#FFFFFF'
   },

   grey: {
      color: '#838383'
   },

   theme: {
      color: '#BEAF87'
   }
})