import createReducer from 'app/lib/createReducer'
import * as types from 'app/actions/types'

const initialState = {
   name  : '',
   addr  : '',
   sAddr : ''
}

export const userReducer = createReducer(initialState, {
   [types.USER_ADD](state, action) {
      return {
         ...state,
         nama  : action.nama,
         addr  : action.addr,
         sAddr : action.sAddr
      }
   },

   [types.USER_DELETE](state) {
      return {
         ...state,
         nama  : '',
         addr  : '',
         sAddr : ''
      }
   }
})