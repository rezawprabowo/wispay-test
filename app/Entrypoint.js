import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/es/integration/react'
import Navigator from 'app/navigation'
import configureStore from 'app/store/configureStore'

import SplashScreen from './screens/Splash Screen'

const { persistor, store } = configureStore()

export default class Entrypoint extends Component {
   render() {
      return (
         <Provider store={ store }>
            <PersistGate
               loading={ <SplashScreen /> }
               persistor={ persistor }
            >
               <Navigator />
            </PersistGate>
         </Provider>
      )
   }
}
