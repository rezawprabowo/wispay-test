import NavigationService from 'app/navigation/NavigationService'

export function navigateToProd(prodName, params) {
   NavigationService.navigate(prodName, params)
}

export function navigateToHome(params) {
   NavigationService.navigate('Home', params)
}

export function navigateToDetail(params) {
   NavigationService.navigate('Detail', params)
}

export function goBack(params) {
   NavigationService.goBack(params)
}