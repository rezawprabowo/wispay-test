import * as types from '../types'

export function userAdd(dataArray) {
   return {
      type           : types.USER_ADD,
      cid            : dataArray.cid,
      nama           : dataArray.nama,
      deviceid       : dataArray.deviceid,
      idmerchant     : dataArray.idmerchant,
      uraianmerchant : dataArray.uraianmerchant,
      masterkey      : dataArray.masterkey,
      token          : dataArray.token,
      MenuJenis      : dataArray.MenuJenis
   }
}

export function userDelete() {
   return {
      type           : types.USER_DELETE
   }
}

export function userBalanceSinkron(availablebalance) {
   return {
      type            : types.USER_BALANCE_SINKRON,
      availablebalance: availablebalance
   }
}

export function userInfo(dataArray) {
   return {
      type           : types.USER_INFO,
      du_alamat      : dataArray.du_alamat,
      norek          : dataArray.norek,
   }
}

export function onUserInjection(status) {
   return {
      type           : types.USER_INJECTION,
      inject_status  : status
   }
}