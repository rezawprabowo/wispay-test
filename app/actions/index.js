import * as userActions from './screens/userActions'

import * as navigationActions from './navigationActions'

export const ActionCreators = Object.assign(
   {},
   navigationActions,
   userActions
)
