const images = {
   person: {
      henry    : require('../../assets/person/henry.png'),
      andrea   : require('../../assets/person/andrea.png'),
      stewart  : require('../../assets/person/stewart.png')
   },
   place: {
      branz    : require('../../assets/place/branz.png'),
      bsd      : require('../../assets/place/bsd.png'),
      bsd2     : require('../../assets/place/bsd-sold.png'),
      foresta  : require('../../assets/place/foresta.png'),
      nava     : require('../../assets/place/nava-park.png'),
      woody    : require('../../assets/place/woody.png')
   },
   icon: {
      cert     : require('../../assets/icon/cert.png'),
      clock    : require('../../assets/icon/clock.png'),
      fav      : require('../../assets/icon/fav.png'),
      bed      : require('../../assets/icon/list-bed.png'),
      garage   : require('../../assets/icon/list-garage.png'),
      landmark : require('../../assets/icon/list-landmark.png'),
      loc      : require('../../assets/icon/loc.png'),
      parking  : require('../../assets/icon/list-parking.png'),
      lPhone   : require('../../assets/icon/list-phone.png'),
      power    : require('../../assets/icon/list-power.png'),
      stair    : require('../../assets/icon/list-stair.png'),
      toilet   : require('../../assets/icon/list-toilet.png'),
      lock     : require('../../assets/icon/lock.png'),
      phone    : require('../../assets/icon/phone.png'),
      whatsapp : require('../../assets/icon/whatsapp.png')
   }
}

export default images