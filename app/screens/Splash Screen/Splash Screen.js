import React from 'react'
import {  View, ActivityIndicator } from 'react-native'
import { Container, Body } from 'native-base'
import styles from './styles'

const SplashScreen = () => {
   return (
      <Container style={ styles.container }>
         <Body>
            <View style={ styles.contentContainer }>
               <ActivityIndicator size='large' color='#000000' />
            </View>
         </Body>
      </Container>
   )
}

export default SplashScreen