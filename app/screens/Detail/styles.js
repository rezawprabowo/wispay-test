import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
   absolute: {
      position: 'absolute'
   },
   
   container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: '#F9F9F9',
      height,
      width
   },
   
   contentContainer: {
      alignItems: 'center',
      justifyContent: 'center',
      width,
      height
   },

   tabBarUnderline: {
      backgroundColor: '#BEAF87'
   },

   tabParent: {
      justifyContent: 'space-evenly'
   },

   tabStyle: {
      paddingLeft: 0,
      paddingRight: 0,
      margin: 0,
      width: '50%',
      backgroundColor: '#FFFFFF'
   },

   textStyle: {
      color: '#000000'
   },

   textStyleActive: {
      color: '#BEAF87'
   },

   noBorder: {
      borderWidth: 0
   },
})

export default styles