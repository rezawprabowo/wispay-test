import React, { useState } from 'react'
import { View, StatusBar, Image, ScrollView, TouchableOpacity, Dimensions } from 'react-native'
import { Icon, Button, Tabs, Tab, ScrollableTab } from 'native-base'
import styles from './styles'

import * as navigationActions from 'app/actions/navigationActions'

import images from 'app/global/images'

import { CompText } from 'app/components'

const { width, height } = Dimensions.get('window')

const formatNumber = (num) => {
   return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

const Listing = (props) => {
   const { data } = props
   const det = data.detail

   return (
      <View style={{ marginTop: 10, paddingHorizontal: 10 }}>
         <View style={{ flexDirection: 'row', marginBottom: 20 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.bed } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='Kamar Tidur' small grey />

                  <CompText text={ det.room } title bold />
               </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.toilet } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='Kamar Mandi' small grey />

                  <CompText text={ det.toilet } title bold />
               </View>
            </View>
         </View>
         
         <View style={{ flexDirection: 'row', marginBottom: 20 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.landmark } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='Luas Bangunan' small grey />

                  <View style={{ flexDirection: 'row' }}>
                     <CompText text={ det.bSize } title bold />
                     <CompText style={{ marginTop: 8 }} text={ det.bMSystem } small bold />
                  </View>
               </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.landmark } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='Luas Tanah' small grey />

                  <View style={{ flexDirection: 'row' }}>
                     <CompText text={ det.size } title bold />
                     <CompText style={{ marginTop: 8 }} text={ det.bMSystem } small bold />
                  </View>
               </View>
            </View>
         </View>
         
         <View style={{ flexDirection: 'row', marginBottom: 20 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.landmark } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='(P x L) Bangunan' small grey />

                  <View style={{ flexDirection: 'row' }}>
                     <CompText text={ det.bLong + ' x ' + det.bWidth } title bold />
                     <CompText style={{ marginTop: 8 }} text={ det.mSystem } small bold />
                  </View>
               </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.landmark } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='(P x L) Tanah' small grey />

                  <View style={{ flexDirection: 'row' }}>
                     <CompText text={ det.long + ' x ' + det.width } title bold />
                     <CompText style={{ marginTop: 8 }} text={ det.mSystem } small bold />
                  </View>
               </View>
            </View>
         </View>
         
         <View style={{ flexDirection: 'row', marginBottom: 20 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.stair } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='Jumlah Lantai' small grey />

                  <CompText text={ det.level } title bold />
               </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.power } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='Listrik' small grey />

                  <View style={{ flexDirection: 'row' }}>
                     <CompText text={ det.power } title bold />
                     <CompText style={{ marginTop: 8 }} text='W' small bold />
                  </View>
               </View>
            </View>
         </View>
         
         <View style={{ flexDirection: 'row', marginBottom: 20 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.garage } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='Kapasitas Garasi' small grey />

                  <CompText text={ det.garage } title bold />
               </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.parking } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='Kapasitas Parkir' small grey />

                  <CompText text={ det.parking } title bold />
               </View>
            </View>
         </View>
         
         <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
               <View style={{ borderRadius: 10, padding: 10, borderColor: '#C0C0C0', borderWidth: 1 }}>
                  <Image style={{ height: 40, width: 40, tintColor: '#000000' }} source={ images.icon.lPhone } resizeMode='contain' />
               </View>

               <View style={{ marginLeft: 10, alignSelf: 'center' }}>
                  <CompText text='Line Telepon' small grey />

                  <CompText text={ det.phone } title bold />
               </View>
            </View>
         </View>

         <View style={{ borderTopWidth: 1, borderTopColor: '#C0C0C0', marginTop: 20, paddingHorizontal: 10, paddingVertical: 20, flexDirection: 'row', alignItems: 'center' }}>
            <Image style={{ height: 0, width: 0 }} source={ images.icon.lPhone } resizeMode='contain' />
            <Image style={{ height: 20, width: 30, marginRight: 10 }} source={ images.icon.cert } resizeMode='contain' />

            <CompText text={ data.ownership ? 'Sertifikat Hak Milik' : 'Tidak Ada Sertifikat' } />
         </View>

         <View style={{ borderTopWidth: 1, borderTopColor: '#C0C0C0', paddingHorizontal: 10, paddingVertical: 20, flexDirection: 'row', alignItems: 'center' }}>
            <Image style={{ height: 0, width: 0 }} source={ images.icon.lPhone } resizeMode='contain' />
            <Image style={{ height: 20, width: 30, marginRight: 10 }} source={ images.icon.clock } resizeMode='contain' />
            
            <CompText text={ 'Diposting pada : ' + data.created_at } />
         </View>
      </View>
   )
}

const Details = (props) => {
   let [ data ] = useState(props.navigation.state.params.data)
   let [ counter, setCounter ] = useState(1)
   let [ page, setPage ] = useState(0)

   function _nextImage() {
      if(counter === data.images.length) {
         setCounter(1)
      }
      else {
         setCounter(counter + 1)
      }
   }

   return (
      <View style={ styles.container }>
         <StatusBar backgroundColor='#000000' />

         <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Button transparent onPress={() => navigationActions.goBack()}>
               <Icon
                  style={{ color: '#000000' }}
                  name='ios-arrow-back'
               />
            </Button>

            <CompText style={{ flex: 1 }} text={ 'Detail Listing #' + data.id } />

            <Icon style={{ marginRight: 10, fontSize: 20 }} name='heart' type='Feather' />
         </View>

         <ScrollView showsVerticalScrollIndicator={ false }>
            <View style={{ elevation: 1, marginBottom: 20, backgroundColor: '#FFFFFF', paddingBottom: 20, borderRadius: 10 }}>
               <TouchableOpacity style={{ position: 'relative' }} onPress={() => _nextImage()}>
                  <Image style={{ marginVertical: 10 }} source={ data.images[counter - 1] } resizeMode='contain' />

                  <View style={{ backgroundColor: 'black', opacity: 0.8, position: 'absolute', left: 10, bottom: 20, paddingHorizontal: 5, paddingVertical: 2, borderRadius: 5 }}>
                     <CompText text={ counter + '/' + data.images.length } white bold />
                  </View>
               </TouchableOpacity>

               <View style={{ marginHorizontal: 20, marginBottom: 10, paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: '#C0C0C0' }}>
                  <CompText text={ data.name } large />

                  <CompText text={ 'Rp ' + formatNumber(data.price) } number bold />
               </View>

               <View style={{ marginHorizontal: 20 }}>
                  <View style={{ flexDirection: 'row' }}>
                     <View style={{ flex: 1 }}>
                        <CompText text='Rumah untuk Dijual' />

                        <View style={{ flexDirection: 'row' }}>
                           <Icon style={{ fontSize: 14, marginRight: 3 }} name='location' type='EvilIcons' />
                           <CompText text={ data.addr } small grey numberOfLines={ 1 } />
                        </View>
                     </View>
                     
                     <Image style={{ height: 30, width: 50 }} source={ images.icon.loc } resizeMode='contain' />
                  </View>
               </View>
            </View>

            <View style={{ elevation: 1, marginBottom: 20, backgroundColor: '#FFFFFF', paddingBottom: 20, borderRadius: 10 }}>
               <Tabs
                  renderTabBar={() =>
                     <ScrollableTab style={ styles.noBorder } underlineStyle={ styles.tabBarUnderline } tabsContainerStyle={ styles.tabParent } />
                  }
                  initialPage={ 0 }
                  page={ page }
                  onChangeTab={({ i }) => setPage(i)}
                  locked
               >
                  <Tab
                     heading='Listing Info'
                     tabStyle={ styles.tabStyle }
                     activeTabStyle={ styles.tabStyle }
                     textStyle={ styles.textStyle }
                     activeTextStyle={ styles.textStyleActive }
                  >
                     <Listing data={ data } />
                  </Tab>

                  <Tab
                     heading='Deskripsi'
                     tabStyle={ styles.tabStyle }
                     activeTabStyle={ styles.tabStyle }
                     textStyle={ styles.textStyle }
                     activeTextStyle={ styles.textStyleActive }
                  >
                     <CompText text='Deskripsi' />
                  </Tab>
               </Tabs>
            </View>

            <View style={{ elevation: 1, marginBottom: 20, backgroundColor: '#FFFFFF', paddingVertical: 20, paddingHorizontal: 10, borderRadius: 10 }}>
               <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                  <Image style={{ height: 20, width: 20, tintColor: '#000000', marginRight: 10 }} source={ images.icon.lock } resizeMode='contain' />

                  <CompText text='Catatan Internal' />
               </View>

               <CompText text='Hanya akan dilihat oleh Marketing satu perusahaan.' small theme />

               <CompText style={{ marginTop: 20 }} text='Alamat Details : ' />

               <CompText text={ data.cAddr } />
            </View>

            <View style={{ elevation: 1, marginBottom: 1, backgroundColor: '#FFFFFF', paddingVertical: 20, paddingHorizontal: 10, borderRadius: 10 }}>
               <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                  <Image style={{ height: 20, width: 20, tintColor: '#000000', marginRight: 10 }} source={ images.icon.lock } resizeMode='contain' />

                  <CompText text='Owner Properti' />
               </View>

               <CompText text='Hanya anda yang dapat melihat informasi ini.' small theme />
               
               <View style={{ flexDirection: 'row', marginTop: 20 }}>
                  <CompText text='Nama : ' />
                  <CompText text={ data.owner.name } bold />
               </View>

               <View style={{ flexDirection: 'row' }}>
                  <CompText text='No. HP : ' />
                  <CompText text={ data.owner.phone } bold />
               </View>
            </View>
         </ScrollView>

         <View style={{ width, paddingHorizontal: 40, paddingVertical: 15 }}>
            <TouchableOpacity style={{ backgroundColor: 'black', borderRadius: 5, paddingVertical: 15, alignItems: 'center' }}>
               <View style={{ flexDirection: 'row' }}>
                  <Icon style={{ fontSize: 18, color: 'white' }} name='share-square-o' type='FontAwesome' />

                  <CompText style={{ marginLeft: 10 }} text='Promosikan' bold white />
               </View>
            </TouchableOpacity>
         </View>
      </View>
   )
}

export default Details