import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
   absolute: {
      position: 'absolute'
   },

   container: {
      flex: 1,
      backgroundColor: '#FFFFFF',
      width
   },
   
   containerContent: {
      paddingBottom: 50,
      backgroundColor: '#F9F9F9',
      alignItems: 'center',
      width
   },

   containerCard: {
      elevation: 1,
      marginTop: 20,
      paddingVertical: 10,
      borderRadius: 20,
      width: width - 20,
      backgroundColor: '#FFFFFF'
   },

   containerPhoto: {
      justifyContent: 'center',
      alignItems: 'center',
      marginVertical: 20
   },

   tabBarUnderline: {
      backgroundColor: '#BEAF87'
   },

   tabParent: {
      justifyContent: 'space-evenly'
   },

   tabStyle: {
      paddingLeft: 0,
      paddingRight: 0,
      margin: 0,
      width: '33.3%',
      backgroundColor: '#FFFFFF'
   },

   textStyle: {
      color: '#000000'
   },

   textStyleActive: {
      color: '#BEAF87'
   },

   noBorder: {
      borderWidth: 0
   },

   photoBox: {
      borderWidth: 1,
      borderColor: '#C0C0C0',
      borderRadius: 37.5,
      width: 75,
      height: 75,
      marginBottom: 5,
      padding: 3
   },

   onwerBox: {
      borderWidth: 1,
      borderColor: '#C0C0C0',
      borderRadius: 25,
      width: 50,
      height: 50,
      padding: 3
   },

   whiteBox: {
      position: 'absolute',
      top: '40%',
      left: '37.5%',
      borderRadius: 5,
      backgroundColor: '#FFFFFF',
      opacity: 0.9,
      transform: [
         { rotate: '-15deg' }
      ]
   },

   terjual: {
      borderWidth: 2,
      borderColor: '#BEAF87',
      borderStyle: 'dashed',
      borderRadius: 5,
      paddingVertical: 10,
      paddingHorizontal: 20
   },

   modal: {
      alignItems: 'center', 
      justifyContent: 'flex-end',
      margin: 0,
      padding: 0
   },
   
   modalContainer: {
      backgroundColor: '#FFFFFF',
      width: '100%',
      paddingVertical: 20,
      paddingHorizontal: 40,
      height: 'auto',
      borderRadius: 20,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
      alignItems: 'center',
      justifyContent: 'center',
      flex: 0
   },
 
   button: { 
      paddingHorizontal: 20,
      elevation: 0, 
      height: 40 
   },
 
   buttonLight: {
      backgroundColor: 'transparent', 
      borderColor: '#C0C0C0', 
      borderWidth: 1
   }
})

export default styles