import React, { useState, useEffect } from 'react'
import { View, StatusBar, Image, ScrollView, TouchableOpacity } from 'react-native'
import { Tab, Tabs, ScrollableTab, Icon, Button } from 'native-base'
import { connect } from 'react-redux'
import styles from './styles'

import * as navigationActions from 'app/actions/navigationActions'

import images from 'app/global/images'

import { CompText } from 'app/components'
import Modal from 'react-native-modal'

const formatNumber = (num) => {
   return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

const ModalOption = (props) => {
   const { visible, closeModal } = props

   return (
      <Modal
         onBackdropPress={ closeModal }
         isVisible={ visible }
         backdropOpacity={ 0.9 }
         style={ styles.modal }
      >
         <View style={ styles.modalContainer }>
            <TouchableOpacity style={{ flexDirection: 'row', padding: 20,  width: '100%', alignItems: 'center' }} onPress={() => alert('Terubah')}>
               <Icon style={{ fontSize: 20, marginRight: 20 }} name='pencil' type='SimpleLineIcons' />

               <CompText text='Ubah' />
            </TouchableOpacity>

            <View style={{ borderBottomColor: '#C0C0C0', borderBottomWidth: 1,  width: '100%' }} />

            <TouchableOpacity style={{ flexDirection: 'row', padding: 20, width: '100%', alignItems: 'center' }} onPress={() => alert('Terhapus')}>
               <Icon style={{ fontSize: 20, marginRight: 20 }} name='trashcan' type='Octicons' />

               <CompText text='Hapus' />
            </TouchableOpacity>

            <View style={{ borderBottomColor: '#C0C0C0', borderBottomWidth: 1,  width: '100%' }} />

            <TouchableOpacity style={{ flexDirection: 'row', padding: 20, width: '100%', alignItems: 'center' }} onPress={() => alert('Terjual')}>
               <Icon style={{ fontSize: 20, marginRight: 20 }} name='pricetag-outline' type='Ionicons' />

               <CompText text='Tandai Terjual' />
            </TouchableOpacity>
         </View>
      </Modal>
   )
}

const Owned = (props) => {
   const { data, owner, openModal } = props

   const statusColor = (str) => {
      if(str === 'Disewa') {
         return '#5497F1'
      }
      else {
         return '#71C392'
      }
   }

   return (
      <ScrollView contentContainerStyle={ styles.containerContent }>
         { data.map((o) => (
            <TouchableOpacity style={ styles.containerCard } key={ o.id } onPress={() => navigationActions.navigateToDetail({ data: o })}>
               <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                  <View style={ styles.onwerBox }>
                     <Image
                        style={{ flex: 1, width: null, height: null }}
                        source={ images.person.henry }
                        resizeMode='contain'
                     />
                  </View>

                  <View style={{ flex: 1, justifyContent: 'center', marginLeft: 10 }}>
                     <CompText text={ owner.name } bold />

                     <CompText text={ owner.sAddr } small />
                  </View>

                  <Button transparent onPress={ openModal }>
                     <Icon
                        style={{ color: '#000000' }}
                        name='dots-vertical' type='MaterialCommunityIcons'
                     />
                  </Button>
               </View>

               <Image
                  style={{ marginVertical: 10, width: '100%', height: undefined, aspectRatio: 200 / 76 }}
                  source={ o.image }
                  resizeMode='contain'
               />

               <View style={{ marginHorizontal: 10, borderBottomWidth: 1, borderBottomColor: '#F2F2F2', paddingBottom: 5 }}>
                  <CompText text={ o.name } numberOfLines={ 1 } />

                  <CompText text={ o.status === 'Dijual' ? 'Rp ' + formatNumber(o.price) : 'Rp ' + formatNumber(o.price) + '/bulan' } bold number />

                  <View style={{ flexDirection: 'row' }}>
                     <CompText text={ o.type } bold />

                     <View style={{ marginLeft: 10, padding: 5, paddingHorizontal: 10, borderRadius: 5, backgroundColor: statusColor(o.status) }}>
                        <CompText text={ o.status } bold small white />
                     </View>
                  </View>

                  <CompText text={ o.addr } numberOfLines={ 1 } small grey />
               </View>

               <View style={{ flexDirection: 'row', paddingHorizontal: 10, paddingTop: 10 }}>
                  <View style={{ alignSelf: 'flex-start', borderRightWidth: 1, width: '33%' }}>
                     <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: 30, tintColor: 'grey' }} source={ images.icon.bed } resizeMode='contain' />

                        <CompText text={ o.detail.room } title bold />
                     </View>

                     <CompText style={{ marginLeft: 10 }} text='K. Tidur' small grey />
                  </View>

                  <View style={{ alignSelf: 'flex-start', borderRightWidth: 1, width: '33%' }}>
                     <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: 30, tintColor: 'grey' }} source={ images.icon.toilet } resizeMode='contain' />

                        <CompText text={ o.detail.toilet } title bold />
                     </View>

                     <CompText style={{ marginLeft: 10 }} text='K. Mandi' small grey />
                  </View>

                  <View style={{ alignSelf: 'flex-start', width: '33%' }}>
                     <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: 30, tintColor: 'grey' }} source={ images.icon.landmark } resizeMode='contain' />

                        <View style={{ flexDirection: 'row' }}>
                           <CompText text={ o.detail.size } title bold />
                           <CompText style={{ marginTop: 8 }} text={ o.detail.mSystem } small bold />
                        </View>
                     </View>

                     <CompText style={{ marginLeft: 10 }} text='L. Tanah' small grey />
                  </View>
               </View>
            </TouchableOpacity>
            ))
         }
      </ScrollView>
   )
}

const Favorite = (props) => {
   const { data, openModal } = props

   const statusColor = (str) => {
      if(str === 'Disewa') {
         return '#5497F1'
      }
      else {
         return '#71C392'
      }
   }

   return (
      <ScrollView contentContainerStyle={ styles.containerContent }>
         { data.map((o) => (
            <View style={ styles.containerCard } key={ o.id }>
               <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                  <View style={ styles.onwerBox }>
                     <Image
                        style={{ flex: 1, width: null, height: null }}
                        source={ o.pp }
                        resizeMode='contain'
                     />
                  </View>

                  <View style={{ flex: 1, justifyContent: 'center', marginLeft: 10 }}>
                     <CompText text={ o.owner } bold />

                     <CompText text={ o.addr } small />
                  </View>

                  <Button transparent onPress={ openModal }>
                     <Icon
                        style={{ color: '#000000' }}
                        name='dots-vertical' type='MaterialCommunityIcons'
                     />
                  </Button>
               </View>

               <Image
                  style={{ marginVertical: 10, width: '100%', height: undefined, aspectRatio: 200 / 76 }}
                  source={ o.image }
                  resizeMode='contain'
               />

               <View style={{ marginHorizontal: 10, borderBottomWidth: 1, borderBottomColor: '#F2F2F2', paddingBottom: 5 }}>
                  <CompText text={ o.name } numberOfLines={ 1 } />

                  <CompText text={ o.status === 'Dijual' ? 'Rp ' + formatNumber(o.price) : 'Rp ' + formatNumber(o.price) + '/bulan' } bold number />

                  <View style={{ flexDirection: 'row' }}>
                     <CompText text={ o.type } bold />

                     <View style={{ marginLeft: 10, padding: 5, paddingHorizontal: 10, borderRadius: 5, backgroundColor: statusColor(o.status) }}>
                        <CompText text={ o.status } bold small white />
                     </View>
                  </View>

                  <CompText text={ o.addr } numberOfLines={ 1 } small grey />
               </View>

               <View style={{ flexDirection: 'row', paddingHorizontal: 10, paddingTop: 10 }}>
                  <View style={{ alignSelf: 'flex-start', borderRightWidth: 1, width: '33%' }}>
                     <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: 30, tintColor: 'grey' }} source={ images.icon.bed } resizeMode='contain' />

                        <CompText text={ o.detail.room } title bold />
                     </View>

                     <CompText style={{ marginLeft: 10 }} text='K. Tidur' small grey />
                  </View>

                  <View style={{ alignSelf: 'flex-start', borderRightWidth: 1, width: '33%' }}>
                     <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: 30, tintColor: 'grey' }} source={ images.icon.toilet } resizeMode='contain' />

                        <CompText text={ o.detail.toilet } title bold />
                     </View>

                     <CompText style={{ marginLeft: 10 }} text='K. Mandi' small grey />
                  </View>

                  <View style={{ alignSelf: 'flex-start', width: '33%' }}>
                     <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: 30, tintColor: 'grey' }} source={ images.icon.landmark } resizeMode='contain' />

                        <View style={{ flexDirection: 'row' }}>
                           <CompText text={ o.detail.size } title bold />
                           <CompText style={{ marginTop: 8 }} text={ o.detail.mSystem } small bold />
                        </View>
                     </View>

                     <CompText style={{ marginLeft: 10 }} text='K. Tidur' small grey />
                  </View>
               </View>
            </View>
            ))
         }
      </ScrollView>
   )
}

const Archive = (props) => {
   const { data, owner, openModal } = props

   const statusColor = (str) => {
      if(str === 'Disewa') {
         return '#5497F1'
      }
      else {
         return '#71C392'
      }
   }

   return (
      <ScrollView contentContainerStyle={ styles.containerContent }>
         { data.map((o) => (
            <View style={ styles.containerCard } key={ o.id }>
               <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                  <View style={ styles.onwerBox }>
                     <Image
                        style={{ flex: 1, width: null, height: null }}
                        source={ images.person.henry }
                        resizeMode='contain'
                     />
                  </View>

                  <View style={{ flex: 1, justifyContent: 'center', marginLeft: 10 }}>
                     <CompText text={ owner.name } bold />

                     <CompText text={ owner.sAddr } small />
                  </View>

                  <Button transparent onPress={ openModal }>
                     <Icon
                        style={{ color: '#000000' }}
                        name='dots-vertical' type='MaterialCommunityIcons'
                     />
                  </Button>
               </View>

               <View style={{ position: 'relative' }}>
                  <Image
                     style={{ marginVertical: 10, width: '100%', height: undefined, aspectRatio: 200 / 76 }}
                     source={ o.image }
                     resizeMode='contain'
                  />

                  <View style={ styles.whiteBox }>
                     <View style={ styles.terjual }>
                        <CompText style={ styles.textStyleActive } text={ o.status === 'Dijual' ? 'Terjual' : 'Tersewa' } bold />
                     </View>
                  </View>
               </View>

               <View style={{ marginHorizontal: 10, borderBottomWidth: 1, borderBottomColor: '#F2F2F2', paddingBottom: 5 }}>
                  <CompText text={ o.name } numberOfLines={ 1 } />

                  <CompText text={ o.status === 'Dijual' ? 'Rp ' + formatNumber(o.price) : 'Rp ' + formatNumber(o.price) + '/bulan' } bold number />

                  <View style={{ flexDirection: 'row' }}>
                     <CompText text={ o.type } bold />

                     <View style={{ marginLeft: 10, padding: 5, paddingHorizontal: 10, borderRadius: 5, backgroundColor: statusColor(o.status) }}>
                        <CompText text={ o.status } bold small white />
                     </View>
                  </View>

                  <CompText text={ o.addr } numberOfLines={ 1 } small grey />
               </View>

               <View style={{ flexDirection: 'row', paddingHorizontal: 10, paddingTop: 10 }}>
                  <View style={{ alignSelf: 'flex-start', borderRightWidth: 1, width: '33%' }}>
                     <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: 30, tintColor: 'grey' }} source={ images.icon.bed } resizeMode='contain' />

                        <CompText text={ o.detail.room } title bold />
                     </View>

                     <CompText style={{ marginLeft: 10 }} text='K. Tidur' small grey />
                  </View>

                  <View style={{ alignSelf: 'flex-start', borderRightWidth: 1, width: '33%' }}>
                     <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: 30, tintColor: 'grey' }} source={ images.icon.toilet } resizeMode='contain' />

                        <CompText text={ o.detail.toilet } title bold />
                     </View>

                     <CompText style={{ marginLeft: 10 }} text='K. Mandi' small grey />
                  </View>

                  <View style={{ alignSelf: 'flex-start', width: '33%' }}>
                     <View style={{ flexDirection: 'row' }}>
                        <Image style={{ height: 30, tintColor: 'grey' }} source={ images.icon.landmark } resizeMode='contain' />

                        <View style={{ flexDirection: 'row' }}>
                           <CompText text={ o.detail.size } title bold />
                           <CompText style={{ marginTop: 8 }} text={ o.detail.mSystem } small bold />
                        </View>
                     </View>

                     <CompText style={{ marginLeft: 10 }} text='K. Tidur' small grey />
                  </View>
               </View>
            </View>
            ))
         }
      </ScrollView>
   )
}

const Home = (props) => {
   let [ listOwned, setListowned ] = useState([])
   let [ listFavorite, setListfavorite ] = useState([])
   let [ listArchive, setListarchive ] = useState([])
   let [ loginData, setLogindata ] = useState([])
   let [ page, setPage ] = useState(0)
   let [ modal, setModal ] = useState(false)

   useEffect(() => {
      _login()
      _getList()
      _getFavorit()
      _getArchive()
   }, [])

   function _login() {
      const login = {
         name  : 'Henry Scott',
         addr  : 'Member Broker Century 21 BSD City',
         sAddr : 'Century 21 BSD City'
      }

      setLogindata(login)
   }

   function _getList() {
      const getList = [
         {
            id          : 1,
            name        : 'Nava Park BSD City',
            addr        : 'Jl. Edutown Kav. III 1, BSD, Tangerang Selatan',
            cAddr       : 'NavaPark Cluster Moonlight Blok 38D No. 55',
            image       : images.place.nava,
            type        : 'Rumah',
            status      : 'Dijual',
            price       : 6500000000,
            ownership   : true,
            created_at  : '24-Dec-2019',
            detail      : {
               room     : '3+1',
               toilet   : '3+1',
               size     : 300,
               bSize    : 250,
               bMSystem : 'M2',
               long     : 10,
               bLong    : 10,
               width    : 30,
               bWidth   : 25,
               level    : 2,
               power    : 3000,
               garage   : 2,
               parking  : 2,
               phone    : 2,
               mSystem  : 'M'
            },
            owner       : {
               name     : 'Jois Aprillio',
               phone    : '081388809999'
            },
            images      : [
               images.place.nava,
               images.place.woody,
               images.place.nava,
               images.place.woody,
               images.place.nava
            ]
         },
         {
            id          : 2,
            name        : 'Woody Residence Foresta',
            addr        : 'Jl. Edutown Kav III.1, BSD, Tangerang Selatan',
            cAddr       : 'NavaPark Cluster Moonlight Blok 38D No. 55',
            image       : images.place.woody,
            type        : 'Apartemen',
            status      : 'Disewa',
            price       : 10000000,
            ownership   : true,
            created_at  : '24-Dec-2019',
            detail      : {
               room     : '4',
               toilet   : '4',
               size     : 275,
               bSize    : 250,
               bMSystem : 'M2',
               long     : 10,
               bLong    : 10,
               width    : 30,
               bWidth   : 25,
               level    : 2,
               power    : 3000,
               garage   : 2,
               parking  : 2,
               phone    : 2,
               mSystem  : 'M'
            },
            owner    : {
               name     : 'Jois Aprillio',
               phone    : '081388809999'
            },
            images      : [
               images.place.woody,
               images.place.nava,
               images.place.woody,
               images.place.nava,
               images.place.woody
            ]
         }
      ]

      setListowned(getList)
   }

   function _getFavorit() {
      const getList = [
         {
            id       : 1,
            pp       : images.person.andrea,
            owner    : 'Andrea Colins',
            addr     : 'Century 21 BSD City',
            name     : 'B Residence BSD Signature Lotus',
            addr     : 'Jl. Edutown Kav III.1, BSD, Tangerang Selatan',
            image    : images.place.bsd,
            type     : 'Rumah',
            status   : 'Dijual',
            price    : 2500000000,
            detail   : {
               room     : '3+1',
               toilet   : '2+1',
               size     : 118,
               mSystem  : 'M'
            }
         },
         {
            id       : 2,
            pp       : images.person.stewart,
            owner    : 'Stewart Vale',
            addr     : 'Century 21 Breeze',
            name     : 'The Branz Apartment BSD City',
            addr     : 'Jl. Edutown Kav III.1, BSD, Tangerang Selatan',
            image    : images.place.branz,
            type     : 'Rumah',
            status   : 'Dijual',
            price    : '2250000000',
            detail   : {
               room     : '3+1',
               toilet   : '2+1',
               size     : 85,
               mSystem  : 'M'
            }
         }
      ]

      setListfavorite(getList)
   }

   function _getArchive() {
      const getList = [
         {
            id       : 1,
            name     : 'B Residence BSD Signature Lotus',
            addr     : 'Jl. Edutown Kav III.1, BSD, Tangerang Selatan',
            image    : images.place.bsd2,
            type     : 'Apartemen',
            status   : 'Disewa',
            price    : 4000000,
            detail   : {
               room     : '2',
               toilet   : '2',
               size     : 50,
               mSystem  : 'M'
            }
         },
         {
            id       : 2,
            name     : 'Foresta Ultimo BSD City',
            addr     : 'Jl. Edutown Kav III.1, BSD, Tangerang Selatan',
            image    : images.place.foresta,
            type     : 'Rumah',
            status   : 'Dijual',
            price    : 2500000000,
            detail   : {
               room     : '3+1',
               toilet   : '2+1',
               size     : 118,
               mSystem  : 'M'
            }
         }
      ]

      setListarchive(getList)
   }

   return (
      <View style={ styles.container }>
         <StatusBar backgroundColor='#000000' />

         <View style={ styles.absolute }>
            <Button transparent onPress={() => navigationActions.navigateToHome()}>
               <Icon
                  style={{ color: '#000000' }}
                  name='ios-arrow-back'
               />
            </Button>
         </View>

         <View style={ styles.containerPhoto }>
            <View style={ styles.photoBox }>
               <Image
                  style={{ flex: 1, width: null, height: null }}
                  source={ images.person.henry }
                  resizeMode='contain'
               />
            </View>

            <CompText text={ loginData.name } bold />
            <CompText text={ loginData.addr } small />
         </View>
         
         <Tabs
            renderTabBar={() =>
               <ScrollableTab style={ styles.noBorder } underlineStyle={ styles.tabBarUnderline } tabsContainerStyle={ styles.tabParent } />
            }
            initialPage={ 0 }
            page={ page }
            onChangeTab={({ i }) => setPage(i)}
            locked
         >
            <Tab
               heading='Listing'
               tabStyle={ styles.tabStyle }
               activeTabStyle={ styles.tabStyle }
               textStyle={ styles.textStyle }
               activeTextStyle={ styles.textStyleActive }
            >
               <Owned
                  data={ listOwned }
                  owner={ loginData }
                  openModal={() => setModal(true)}
               />
            </Tab>

            <Tab
               heading='Favorit'
               tabStyle={ styles.tabStyle }
               activeTabStyle={ styles.tabStyle }
               textStyle={ styles.textStyle }
               activeTextStyle={ styles.textStyleActive }
            >
               <Favorite
                  data={ listFavorite }
                  openModal={() => setModal(true)}
               />
            </Tab>
            
            <Tab
               heading='Arsip'
               tabStyle={ styles.tabStyle }
               activeTabStyle={ styles.tabStyle }
               textStyle={ styles.textStyle }
               activeTextStyle={ styles.textStyleActive }
            >
               <Archive
                  data={ listArchive }
                  owner={ loginData }
                  openModal={() => setModal(true)}
               />
            </Tab>
         </Tabs>

         <ModalOption
            visible={ modal }
            closeModal={() => setModal(false)}
         />
      </View>
   )
}

function mapStateToProps(state) {
   return {
      user: state.userReducer
   }
}

export default connect(
   mapStateToProps
)(Home)
